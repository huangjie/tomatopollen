#!/usr/bin/env python

"""
Convert .obo file to tab-delimited file.

Run like this:

makeOboTable.py file.obo > obo.tsv
"""

import fileinput,sys,os

def main():
    id=None
    name=None
    name_space=None
    defn=None
    recs=[]
    for line in fileinput.input():
        line=line.rstrip()
        if line.startswith('[Term]'):
           rec={}
           recs.append(rec)
        if line.startswith('id: '):
            rec['id']=line[len('id: '):]
        elif line.startswith('namespace: '):
            rec['namespace']=line[len('namespace: '):]
        elif line.startswith('name: '):
            rec['name']=line[len('name: '):]
        elif line.startswith('def: "'):
            defn=line.split('"')[1]
            rec['defn']=defn
    heads=['id','namespace','name','defn']
    sys.stdout.write('\t'.join(heads)+'\n')
    for rec in recs:
        toks=map(lambda x:rec[x],heads)
        line='\t'.join(toks)+'\n'
        try:
            line.rindex('OBSOLETE')
            next
        except ValueError:
            if line.startswith('GO:'):
                sys.stdout.write(line)


if __name__ == '__main__':
    main()



